export class Home {

  static load() {
    cy.visit('http://localhost:3000')
  }  

  static P1Init() {
    cy.get('[data-testid="link"]').should('have.text', 'Learn React');
    const counterValue = cy.get('[data-testid="value"]')
    counterValue.should('have.text', '0')  
    cy.get('[data-testid="increment"]').click();
    counterValue.should('not.eq', '0');
  }

  static P2Action() {
    cy.get('[data-testid="increment"]').click();
    cy.get('[data-testid="value"]').should('not.eq', '0');
  }
}