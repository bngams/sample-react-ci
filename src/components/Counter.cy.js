import React from 'react'
import { Counter, IncrementManager } from './Counter';

describe('<Counter />', () => {
  it('renders', () => {
    // see: https://on.cypress.io/mounting-react
    cy.mount(<Counter />);
    cy.get('[data-testid=value]').should('have.text', '0');
    // stub on Increment manager 
    cy.stub(IncrementManager, 'getIncrement').returns(1);
    // spy on logger
    cy.spy(console, 'log')
    cy.get('[data-testid="increment"]').click();
    // is spy called
    // expect(console.log).to.be.called
    cy.get('[data-testid=value]').should('have.text', '1');
    cy.get('[data-testid="decrement"]').click();
    cy.get('[data-testid=value]').should('have.text', '0');
  })
})