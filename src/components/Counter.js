import React, { useState } from 'react';

export class IncrementManager {
    static getIncrement() {
        // simulate big logic, get data from db or other ws
        return Math.random();
    }
}

export const Counter = () => {

    const [counter, setCounter] = useState(0);

    const incrementCounter = () => {
        // => spy
        // Logger.info()....
        console.log('increment'); 

        // stub
        const i = IncrementManager.getIncrement();

        setCounter((prevCounter) => prevCounter + i);
    };

    const decrementCounter = () => {
        setCounter((prevCounter) => prevCounter - 1);
    };

    return(
        <>
        <h1>Counter</h1>
        <button id="incrementBtn" data-testid="increment" onClick={incrementCounter}>+</button>
        <p data-testid="value">{counter}</p>
        <button data-testid="decrement" onClick={decrementCounter}>-</button>
        </>
    );
}