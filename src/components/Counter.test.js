import { render, screen, fireEvent } from '@testing-library/react';
import { Counter, IncrementManager } from './Counter';

describe('Counter testing', () => {
    test('increments counter', () => {
        // get counter to 0;
        render(<Counter />);

        // stub on Increment manager 
        cy.stub(IncrementManager, 'getIncrement').returns(1)

        // spy on logger
        cy.spy(console, 'log')

        // get button element
        const incrementBtn = screen.getByTestId('increment');
        // click on + button
        fireEvent.click(incrementBtn);

        // is spy called 
        expect(console.log).to.be.called

        // get counter text
        const counterText = screen.getByTestId('value');
        // assertion        
        expect(counterText).toHaveTextContent('1');
    });
});