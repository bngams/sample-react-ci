import React from 'react'
import Todos from './Todos'

describe('<Todos />', () => {
  it('renders', () => {

    // Mock
    cy.intercept('/todos', { fixture: 'todos' }).as('getTodos')
    
    // see: https://on.cypress.io/mounting-react
    cy.mount(<Todos />)

    // not mandatory here as request is called on component init
    // cy.wait(['@getTodos'])

    
    cy.get('[data-testid=todo]').should('have.length', 3)
  })
})