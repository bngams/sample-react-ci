import axios from 'axios';
import React, { useState, useEffect } from 'react';

const Todos = () => {

    const [todoList, setTodoList] = useState(null);

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/todos').then(todos => {
            setTodoList(todos.data);
        })
    }, []);

    return todoList ? (
        <ul data-testid="todolist">
        { todoList.map((todo, index) =>
            <li key={index} data-testid="todo">{todo.title}</li>
        )}
        </ul>
    ) : (
        <p>Loading...</p>
    );
};

export default Todos;