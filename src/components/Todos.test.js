import { render, screen } from '@testing-library/react';
import Todos from './Todos';
import axios from 'axios';
import TODOS from './Todos.mock';

jest.mock('axios');

describe('Todos testing', () => {

    // mock for all tests
    // beforeEach(() => {
    //     axios.get.mockResolvedValue([...]);
    // })

    test('load todos', async () => {
        render(<Todos />);

        // HOW TO? mock axios specific url
        // jest.mock('axios', () => ({ 
        //     get: jest.fn(url => { 
        //         if (url === 'https://jsonplaceholder.typicode.com/todos') { 
        //             return Promise.resolve({ data: DATA }); 
        //         } 
        //         return Promise.resolve({});
        //     }), 
        // })); 

        // mock axios get 
        axios.get.mockResolvedValue({ data: TODOS});

        // get todos elements
        const todos = await screen.findAllByTestId('todo');

        // get total todos 
        expect(todos).toHaveLength(3);
    });
});